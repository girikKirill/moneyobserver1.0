//
//  HomeView.swift
//  MoneyObserver
//
//  Created by Кирилл on 8.07.21.
//

import UIKit
import SideMenu

class HomeView: UIViewController {

    @IBOutlet weak var costCategoriesCollectionView: UICollectionView!
    @IBOutlet weak var availableFinanceCV: UICollectionView!
    @IBOutlet weak var financeCategoriesCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var balanceInformationView: UIView!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var incomeLabel: UILabel!
    @IBOutlet weak var costsLabel: UILabel!
    
    var costCategories : [CostCategories] = []
    var financeCategories:[FinanceCategories] = []
    let initialCategory = InitialCategories()
    
    var menu: SideMenuNavigationController?
    
    var fromIndex = 0
    var toIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        availableFinanceCV.setupDelegateData(self)
        availableFinanceCV.registerCell(AvailableFinanceCell.self)
        costCategoriesCollectionView.setupDelegateData(self)
        costCategoriesCollectionView.registerCell(CostCategoriesCell.self)
        availableFinanceCV.showsHorizontalScrollIndicator = false
        availableFinanceCV.dragDelegate = self
        availableFinanceCV.dropDelegate = self
        availableFinanceCV.dragInteractionEnabled = true
        costCategoriesCollectionView.dropDelegate = self
        
        availableFinanceCV.layer.cornerRadius = 20
        costCategoriesCollectionView.layer.cornerRadius = 20
        balanceInformationView.layer.cornerRadius = 20
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        title = "Money Observer"
        navigationController?.navigationBar.tintColor = .black
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        menu = SideMenuNavigationController(rootViewController: MenuListController())
        menu?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu
        //SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        //initialCategories()
        self.navigationItem.rightBarButtonItem?.image = UIImage(named: "addMoney")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initialCategories()
        availableFinanceCV.reloadData()
        costCategoriesCollectionView.reloadData()
        setupBalanceView()
    }
    
    func countOfCells() -> Int {
        if costCategories.count % 4 == 0 {
            return costCategories.count / 4
        } else {
            return costCategories.count / 4 + 1
        }
    }
    
    // Setup initial values
    
    func initialCategories() {
        if RealmManager.shared.readFinanceCategories().isEmpty {
            RealmManager.shared.writeFinance(category: initialCategory.wallet)
            RealmManager.shared.writeFinance(category: initialCategory.card)
        }
        financeCategories = RealmManager.shared.readFinanceCategories()
        if RealmManager.shared.readCostCategories().isEmpty {
            RealmManager.shared.writeCost(category: initialCategory.food)
            RealmManager.shared.writeCost(category: initialCategory.transport)
            RealmManager.shared.writeCost(category: initialCategory.services)
            RealmManager.shared.writeCost(category: initialCategory.other)
            RealmManager.shared.writeCost(category: initialCategory.entertainment)
        }
        costCategories = RealmManager.shared.readCostCategories()
    }
    
    func setupBalanceView() {
        let financeOperations = RealmManager.shared.readFinanceOperations()
        var balance = 0.0
        var costs = 0.0
        var income = 0.0
        for item in financeCategories {
            if item.generalBalance {
                balance += item.summ
            }
        }
        for item in financeOperations {
            if item.isSpend {
                costs += item.summ
            } else if !item.isSpend && !item.isTransfer {
                income += item.summ
            }
        }
        balanceLabel.text = "\(balance.format()) BYN"
        costsLabel.text = "\(costs.format()) BYN"
        incomeLabel.text = "\(income.format()) BYN"
    }
    
    private func dragItem(at indexPath: IndexPath) -> [UIDragItem] {
        if let itemCell = availableFinanceCV.cellForItem(at: indexPath) as? AvailableFinanceCell, let text = itemCell.financeName.text as NSString? {
            fromIndex = indexPath.row
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: text))
            dragItem.localObject = text
            dragItem.previewProvider = { () -> UIDragPreview? in
                let imageView = UIImageView(image: UIImage(systemName: "dollarsign.circle"))
                imageView.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
                imageView.tintColor = .white
                imageView.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
                imageView.layer.cornerRadius = 35
                return UIDragPreview(view: imageView)
            }
            return [dragItem]
        } else {
            return []
        }
    }
    
    func transferOfFinance(category: TypesOfCategoryes, fromIndexPath: Int, toIndexPath: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let popUp = storyboard.instantiateViewController(identifier: String(describing: PopUpFinanceOperationView.self)) as? PopUpFinanceOperationView else { return }
        popUp.modalPresentationStyle = .overFullScreen
        popUp.modalTransitionStyle = .crossDissolve
        present(popUp, animated: true, completion: nil)
        popUp.delegate = self
        switch category {
        case .finances:
            if toIndexPath == financeCategories.count { return }
            popUp.type = .finances
            popUp.setupFinOperationsPopUp(fromWhere: financeCategories[fromIndex].name, whereTo: financeCategories[toIndex].name, isTransfer: true, isSpend: false)
        case .costs:
            if toIndexPath == costCategories.count { return }
            popUp.type = .costs
            popUp.setupFinOperationsPopUp(fromWhere: financeCategories[fromIndex].name, whereTo: costCategories[toIndex].name, isTransfer: false, isSpend: true)
        }
    }
    
    @IBAction func menuButtonAction(_ sender: UIBarButtonItem) {
        present(menu!, animated: true, completion: nil)
    }
    
    @IBAction func pushAddMoneyView(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: String(describing: AddMoney.self)) as? AddMoney  else { return }
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
    }
}

// Extensions
// CollectionView FlowLayout

extension HomeView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.setupSizeForItem(collectionView)
        financeCategoriesCollectionHeight.constant = width + 20
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.frame.width < 375 {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        } else {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

// CollectionView delegate & dataSource

extension HomeView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == availableFinanceCV {
            print(indexPath.row)
        }
    }
}

extension HomeView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == self.availableFinanceCV) {
            return financeCategories.count + 1
        } else {
            return costCategories.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if (collectionView == self.availableFinanceCV) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvailableFinanceCell.self), for: indexPath)
            guard let financeCell = cell as? AvailableFinanceCell else { return cell }
            if indexPath.row == financeCategories.count {
                financeCell.isNewCategory = true
                financeCell.setupNewCategoryCell()
            } else {
                financeCell.setupCell(category: financeCategories[indexPath.row])
            }
            financeCell.delegate = self
            return financeCell
            
        } else if (collectionView == self.costCategoriesCollectionView) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CostCategoriesCell.self), for: indexPath)
            guard let categoriesCell = cell as? CostCategoriesCell else { return cell }
            if indexPath.row == costCategories.count {
                categoriesCell.isNewCategory = true
                categoriesCell.setupNewCategoryCell()
            } else {
                categoriesCell.setupCell(category: costCategories[indexPath.row])
            }
            categoriesCell.delegate = self
            return categoriesCell
            
        } else {
            return UICollectionViewCell()
        }
    }
}

// Drag & Drop Delegates

extension HomeView: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if collectionView == availableFinanceCV {
            //session.localContext = collectionView
            
            return dragItem(at: indexPath)
        }else {
            return []
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, dragPreviewParametersForItemAt indexPath: IndexPath) -> UIDragPreviewParameters? {
//        let previewParameters = UIDragPreviewParameters()
//        previewParameters.backgroundColor = .clear
//
//        return previewParameters
//    }
}

extension HomeView: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
            print(destinationIndexPath)
            print(destinationIndexPath.row)
            toIndex = destinationIndexPath.row
            print("первое")
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1, section: 0)
            print(destinationIndexPath)
            print("второе")
        }
        if coordinator.proposal.operation == .move {
            print("ну что-то такое")
            if collectionView == availableFinanceCV {
                if fromIndex == toIndex || toIndex == financeCategories.count {
                    return
                }
                print("finance")
                transferOfFinance(category: .finances, fromIndexPath: fromIndex, toIndexPath: toIndex)
            } else {
                if toIndex == costCategories.count {
                    return
                }
                print("cost")
                transferOfFinance(category: .costs, fromIndexPath: fromIndex, toIndexPath: toIndex)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .move)
    }
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        if collectionView == availableFinanceCV {
            print("finance collection drop session did end")
        } else {
            print("cost collection drop session did end")
        }
    }
}

// Protocols

extension HomeView: OpenView {
    func pushView(forCategory: TypesOfCategoryes) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: String(describing: NewCategoryView.self)) as? NewCategoryView else { return }
        vc.setupView(forCategoy: forCategory)
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    func openFinanceOperationView(fromCategory: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: String(describing: FinanceOperationsView.self)) as? FinanceOperationsView else { return }
        
        vc.setupNumberOfSections(nameOfCategory: fromCategory)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeView: SaveCategories {
    func saveNewFinance(category: FinanceCategories) {
        RealmManager.shared.writeFinance(category: category)
        financeCategories = RealmManager.shared.readFinanceCategories()
        availableFinanceCV.reloadData()
    }
    
    func saveNewCost(category: CostCategories) {
        RealmManager.shared.writeCost(category: category)
        costCategories = RealmManager.shared.readCostCategories()
        costCategoriesCollectionView.reloadData()
    }
}

extension HomeView: PopUpDelegate {
    func cancelAction() {
        print("cancel")
    }
    func saveAction(type: TypesOfCategoryes?, operation: FinancialOperations, indexPath: Int?) {
        switch type {
        case .costs:
            RealmManager.shared.writeFinanceOperations(category: operation)
            let finCategory = FinanceCategories(name: financeCategories[fromIndex].name,
                                                image: financeCategories[fromIndex].image,
                                                summ: financeCategories[fromIndex].summ - operation.summ,
                                                generalBalance: financeCategories[fromIndex].generalBalance)
            let costCategory = CostCategories(name: costCategories[toIndex].name,
                                             image: costCategories[toIndex].image,
                                             summ: costCategories[toIndex].summ + operation.summ,
                                             plannedToSpend: costCategories[toIndex].plannedToSpend)
            RealmManager.shared.FinanceBalanceChange(category: financeCategories[fromIndex], newData: finCategory)
            RealmManager.shared.addPaymentToCategory(category: costCategories[toIndex], newData: costCategory)
            setupBalanceView()
        case .finances:
            RealmManager.shared.writeFinanceOperations(category: operation)
            let finCategoryFrom = FinanceCategories(name: financeCategories[fromIndex].name,
                                                image: financeCategories[fromIndex].image,
                                                summ: financeCategories[fromIndex].summ - operation.summ,
                                                generalBalance: financeCategories[fromIndex].generalBalance)
            let finCategoryTo = FinanceCategories(name: financeCategories[toIndex].name,
                                                image: financeCategories[toIndex].image,
                                                summ: financeCategories[toIndex].summ + operation.summ,
                                                generalBalance: financeCategories[toIndex].generalBalance)
            RealmManager.shared.FinanceBalanceChange(category: financeCategories[fromIndex], newData: finCategoryFrom)
            RealmManager.shared.FinanceBalanceChange(category: financeCategories[toIndex], newData: finCategoryTo)
        case .none:
            return
        }
        costCategoriesCollectionView.reloadData()
        availableFinanceCV.reloadData()
    }
}
