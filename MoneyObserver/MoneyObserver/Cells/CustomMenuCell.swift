//
//  CustomMenuCell.swift
//  MoneyObserver
//
//  Created by Кирилл on 20.07.21.
//

import UIKit

class CustomMenuCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupCell(text: String) {
        mainView.layer.cornerRadius = 15
        label.text = text
    }
    
}
