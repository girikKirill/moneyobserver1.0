//
//  AddMoney.swift
//  MoneyObserver
//
//  Created by Кирилл on 14.07.21.
//

import UIKit

class AddMoney: UIViewController {

    @IBOutlet weak var collectionVIew: UICollectionView!
    
    var data = RealmManager.shared.readFinanceCategories()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionVIew.setupDelegateData(self)
        collectionVIew.registerCell(AvailableFinanceCell.self)

//        let gesture = UILongPressGestureRecognizer(target: self,
//                                                   action: #selector(handleLongPressGesture(_:)))
//        collectionVIew.addGestureRecognizer(gesture)
        
    }
    
//    @objc func handleLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
//        switch gesture.state {
//        case .began:
//            guard let targetIndexPath = collectionVIew.indexPathForItem(at: gesture.location(in: collectionVIew)) else { return }
//
//            collectionVIew.beginInteractiveMovementForItem(at: targetIndexPath)
//            let view = UIView(frame: CGRect(x: 100, y: 100, width: 50, height: 50))
//            view.backgroundColor = .red
//        case .changed:
//            collectionVIew.updateInteractiveMovementTargetPosition(gesture.location(in: collectionVIew))
//        case .ended:
//            collectionVIew.endInteractiveMovement()
//        default:
//            collectionVIew.cancelInteractiveMovement()
//        }
//    }
}

extension AddMoney: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let popUp = storyboard.instantiateViewController(identifier: String(describing: PopUpFinanceOperationView.self)) as? PopUpFinanceOperationView else { return }
        popUp.delegate = self
        popUp.indexPath = indexPath.row
        popUp.modalPresentationStyle = .overFullScreen
        popUp.modalTransitionStyle = .crossDissolve
        present(popUp, animated: true, completion: nil)
        popUp.setupFinOperationsPopUp(whereTo: data[indexPath.row].name, isTransfer: false, isSpend: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.setupSizeForItem(collectionView)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.frame.width < 375 {
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        } else {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = data.remove(at: sourceIndexPath.row)
        data.insert(item, at: destinationIndexPath.row)
    }
}

extension AddMoney: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvailableFinanceCell.self), for: indexPath)
        guard let financeCell = cell as? AvailableFinanceCell else { return cell }
        financeCell.setupFromAddMoney(category: data[indexPath.row])
        //size = financeCell.frame.size
        return financeCell
    }
}

extension AddMoney: PopUpDelegate {
    func cancelAction() {
        print("Cancel")
    }
    
    func saveAction(type: TypesOfCategoryes?, operation: FinancialOperations, indexPath: Int?) {
        guard let indexPath = indexPath else { return }
        RealmManager.shared.writeFinanceOperations(category: operation)
        
        let finCategory = FinanceCategories(name: data[indexPath].name, image: data[indexPath].image, summ: data[indexPath].summ + operation.summ, generalBalance: data[indexPath].generalBalance)
        RealmManager.shared.FinanceBalanceChange(category: data[indexPath], newData: finCategory)
        navigationController?.popViewController(animated: true)
    }
}
