//
//  CalculationView.swift
//  MoneyObserver
//
//  Created by Кирилл on 8.07.21.
//

import UIKit

class CalculationView: UIViewController {

    
    @IBOutlet var numbersButton: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//
//        for button in numbersButton {
//            button.layer.cornerRadius = button.layer.frame.height / 2
//            button.layer.borderWidth = 1
//            button.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for button in numbersButton {
            button.layer.cornerRadius = button.layer.frame.width / 2.2
            button.layer.cornerRadius = self.view.frame.width - 20 
            button.layer.borderWidth = 1
            button.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
