//
//  RealmManager.swift
//  MoneyObserver
//
//  Created by Кирилл on 14.07.21.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    let realm = try! Realm()
    
    private init() { }
    
    // Finance
    
    func writeFinance(category: FinanceCategories) {
        try! realm.write {
            realm.add(category)
        }
    }
    
    func readFinanceCategories() -> [FinanceCategories] {
        return Array(realm.objects(FinanceCategories.self))
    }
    
    func FinanceBalanceChange(category: FinanceCategories, newData: FinanceCategories) {
        try! realm.write {
            category.name = newData.name
            category.image = newData.image
            category.summ = newData.summ
            category.generalBalance = newData.generalBalance
        }
    }
    
    // Coast
    
    func writeCost(category: CostCategories) {
        try! realm.write {
            realm.add(category)
        }
    }
    
    func readCostCategories() -> [CostCategories] {
        return Array(realm.objects(CostCategories.self))
    }
    
    func addPaymentToCategory(category: CostCategories, newData: CostCategories) {
        try! realm.write {
            category.name = newData.name
            category.image = newData.image
            category.summ = newData.summ
            category.plannedToSpend = newData.plannedToSpend
        }
    }
    
    // Finance operations
    
    func writeFinanceOperations(category: FinancialOperations) {
        try! realm.write {
            realm.add(category)
        }
    }
    
    func readFinanceOperations() -> [FinancialOperations] {
        return Array(realm.objects(FinancialOperations.self))
    }
}
