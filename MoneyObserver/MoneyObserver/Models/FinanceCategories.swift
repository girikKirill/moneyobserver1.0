//
//  FinanceCategories.swift
//  MoneyObserver
//
//  Created by Кирилл on 14.07.21.
//

import Foundation
import RealmSwift

class FinanceCategories: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var summ: Double = 0.0
    @objc dynamic var generalBalance: Bool = true

    convenience init(name: String, image: String, summ: Double, generalBalance: Bool) {
        self.init()
        self.name = name
        self.image = image
        self.summ = summ
        self.generalBalance = generalBalance
    }
}
