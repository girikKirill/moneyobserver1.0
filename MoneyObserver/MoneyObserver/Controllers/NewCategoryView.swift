//
//  NewCategoryView.swift
//  MoneyObserver
//
//  Created by Кирилл on 13.07.21.
//

import UIKit

enum TypesOfCategoryes {
    case finances
    case costs
}

class NewCategoryView: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var summTF: UITextField!
    @IBOutlet weak var myBalanceLabel: UILabel!
    @IBOutlet weak var balanceSwitch: UISwitch!
    
    weak var delegate: SaveCategories?
    var alertSummMessage = ""
    var category: TypesOfCategoryes = .finances
    var imageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem?.title = ""
        
        summTF.delegate = self
        summTF.keyboardType = .decimalPad
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    func setupView(forCategoy: TypesOfCategoryes) {
        switch forCategoy {
        case .finances:
            view.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
            titleLabel.textColor = .white
            iconButton.layer.borderWidth = 2
            iconButton.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 0.7)
            iconButton.layer.cornerRadius = iconButton.layer.frame.width / 2
            iconButton.tintColor = .white
            nameTF.placeholder = "Введите название"
            summTF.placeholder = "Остаток на счете"
            myBalanceLabel.textColor = .white
            alertSummMessage = "Остаток на счете должен быть цифрой"
            category = forCategoy

        case .costs:
            view.backgroundColor = #colorLiteral(red: 1, green: 0.7482267022, blue: 0.2770791948, alpha: 1)
            titleLabel.text = "На что вы тратите деньги?"
            titleLabel.textColor = .white
            iconButton.layer.borderWidth = 2
            iconButton.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 0.7)
            iconButton.tintColor = .white
            iconButton.layer.cornerRadius = iconButton.layer.frame.width / 2
            nameTF.placeholder = "Введите название"
            summTF.placeholder = "Планирую тратить"
            myBalanceLabel.textColor = .white
            alertSummMessage = "Планируемая трата должена быть цифрой"
            category = forCategoy
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 120
            }
//        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        guard let name = nameTF.text, !name.isEmpty else {
            let alert = UIAlertController(title: "", message: "Введите название", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Хорошо", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        guard let summ = summTF.text else { return }
        
        switch category {
        case .finances:
//            guard let DoubleSumm = Double(summ) else {
//                let alert = UIAlertController(title: "", message: alertSummMessage, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Хорошо", style: .cancel, handler: { item in
//                    self.summTF.text = ""
//                }))
//                self.present(alert, animated: true, completion: nil)
//                return
//            }
            let formatter = NumberFormatter()
            formatter.decimalSeparator = ","
            let grade = formatter.number(from: summ)
            
            if !summ.isEmpty {
                guard let doubleSumm = grade?.doubleValue, doubleSumm != 0.0 else { return }
                delegate?.saveNewFinance(category: FinanceCategories(name: name, image: imageName, summ: doubleSumm, generalBalance: balanceSwitch.isOn))
            }
            delegate?.saveNewFinance(category: FinanceCategories(name: name, image: imageName, summ: 0.0, generalBalance: balanceSwitch.isOn))
            navigationController?.popViewController(animated: true)
            
        case .costs:
            delegate?.saveNewCost(category: CostCategories(name: name, image: imageName, summ: 0.0, plannedToSpend: 0.0))
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func setIconAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(identifier: String(describing: IconsSetView.self)) as? IconsSetView  else { return }
        vc.categoryType = category
        vc.setupView(categoryType: category)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

// Protocols

extension NewCategoryView: SetImage {
    func saveSelected(image: String) {
        iconButton.setImage(UIImage(named: image), for: .normal)
        iconButton.setTitle("", for: .normal)
        imageName = image
        dismiss(animated: true, completion: nil)
    }
}

extension NewCategoryView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") as NSString
            let newText = text.replacingCharacters(in: range, with: string)
            if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
                return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
            }
            return false
    }
}
