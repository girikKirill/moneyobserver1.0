//
//  MenuListController.swift
//  MoneyObserver
//
//  Created by Кирилл on 13.07.21.
//

import UIKit

enum MenuCategories: String, CaseIterable {
    case Operations = "Операции"
    case Profile = "Профиль"
    case Settings = "Настройки"
}

class MenuListController: UITableViewController {

    
    var items: [MenuCategories] = [.Operations, .Profile, .Settings]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        
        tableView.registerCell(CustomMenuCell.self)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CustomMenuCell.self), for: indexPath)
        guard let customCell = cell as? CustomMenuCell else { return cell}
        customCell.setupCell(text: items[indexPath.row].rawValue)
//        cell.textLabel?.text = items[indexPath.row].rawValue
        customCell.selectionStyle = .none
        return customCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        switch items[indexPath.row] {
        case .Operations:
            guard let vc = storyboard.instantiateViewController(identifier: String(describing: FinanceOperationsView.self)) as? FinanceOperationsView else { return }
            vc.setupNumberOfSections()
            navigationController?.pushViewController(vc, animated: true)
        case .Profile:
            guard let vc = storyboard.instantiateViewController(identifier: String(describing: ProfileView.self)) as? ProfileView else { return }
            navigationController?.pushViewController(vc, animated: true)
        case .Settings:
            guard let vc = storyboard.instantiateViewController(identifier: String(describing: SettingsView.self)) as? SettingsView else { return }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
