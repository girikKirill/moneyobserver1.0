//
//  CostCategoriesCell.swift
//  MoneyObserver
//
//  Created by Кирилл on 12.07.21.
//

import UIKit

class CostCategoriesCell: UICollectionViewCell {

    @IBOutlet weak var categoriesName: UILabel!
    @IBOutlet weak var categoriesImage: UIButton!
    @IBOutlet weak var categoriesSumm: UILabel!
    
    weak var delegate: OpenView?
    var isNewCategory: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoriesImage.layer.cornerRadius = categoriesImage.layer.frame.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetupCell()
        
    }
    
    func initialSetupCell() {
        isNewCategory = false
        categoriesName.isHidden = false
        categoriesSumm.isHidden = false
        categoriesImage.layer.borderWidth = 0
        categoriesImage.backgroundColor = #colorLiteral(red: 1, green: 0.6705882353, blue: 0, alpha: 1)
        categoriesImage.setImage(UIImage(systemName: "pills"), for: .normal)
        categoriesImage.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func setupCell(category: CostCategories) {
        categoriesName.text = category.name
        categoriesSumm.text = "\(category.summ.format())"
        categoriesImage.setImage(UIImage(named: category.image), for: .normal)
    }
    
    func setupNewCategoryCell() {
        isNewCategory = true
        categoriesName.isHidden = true
        categoriesImage.layer.borderWidth = 2
        categoriesImage.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        categoriesImage.backgroundColor = .clear
        categoriesImage.setImage(UIImage(systemName: "plus"), for: .normal)
        categoriesImage.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        categoriesSumm.isHidden = true
    }
    
    @IBAction func pushNewCategoryView(_ sender: Any) {
        if isNewCategory {
            print(isNewCategory)
            isNewCategory = false
            delegate?.pushView(forCategory: .costs)
        } else {
            print(isNewCategory)
            delegate?.openFinanceOperationView(fromCategory: categoriesName.text!)
        }
    }
    
}
