//
//  FinanceOperationsView.swift
//  MoneyObserver
//
//  Created by Кирилл on 20.07.21.
//

import UIKit

class FinanceOperationsView: UIViewController {

    @IBOutlet weak var tableVIew: UITableView!
    
    var sections: [String] = []
    var operations: [FinancialOperations] = RealmManager.shared.readFinanceOperations()
    var operationsByDate: [[FinancialOperations]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableVIew.setupDelegateData(self)
        tableVIew.registerCell(FinanceOperationsCell.self)
        
        operations.reverse()
        
    }
    
    func setupNumberOfSections(nameOfCategory: String? = nil) {
        if let categoryName = nameOfCategory {
            self.title = nameOfCategory
            operations.removeAll()
            let finOperations = RealmManager.shared.readFinanceOperations()
            for item in finOperations {
                if categoryName == item.fromWhere || categoryName == item.whereTo {
                operations.append(item)
                }
            }
        } else {
            title = "Все операции"
        }
        guard let firstDate = operations.first?.date else {
            print("Нет никаких операций")
            return }
        sections.append(firstDate)
        
        var operArry: [FinancialOperations] = []
        
        for item in operations {
            print(item.date)
            if sections.last == item.date {
                operArry.append(item)
                print("дата равна последней в массиве")
            } else {
                operationsByDate.append(operArry)
                operArry.removeAll()
                operArry.append(item)
                sections.append(item.date)
            }
        }
        operationsByDate.append(operArry)
        
        sections.reverse()
        operationsByDate.reverse()
    }
}

extension FinanceOperationsView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width - 10, height: headerView.frame.height - 10)
        label.text = "\(sections[section])"
        label.font = label.font.withSize(20)
        label.textColor = .black
        
        headerView.addSubview(label)
        
        return headerView
    }
}

extension FinanceOperationsView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let financeOperations = operationsByDate[section]
        return financeOperations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FinanceOperationsCell.self), for: indexPath)
        guard let finCell = cell as? FinanceOperationsCell else { return cell }
        let financeOperations = operationsByDate[indexPath.section]
        finCell.setupCell(operation: financeOperations[indexPath.row])
        finCell.selectionStyle = .none
        return finCell
    }
    
    
}
