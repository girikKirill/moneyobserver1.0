//
//  InitialCategories.swift
//  MoneyObserver
//
//  Created by Кирилл on 14.07.21.
//

import Foundation
import UIKit

struct InitialCategories {
    let wallet = FinanceCategories(name: "Кошелек",
                                   image: "wallet2",
                                   summ: 100,
                                   generalBalance: true)
    let card = FinanceCategories(name: "Карта",
                                 image: "creditCard1",
                                 summ: 200,
                                 generalBalance: true)
    let food = CostCategories(name: "Путешевствия",
                              image: "travels1",
                              summ: 0.0,
                              plannedToSpend: 0.0)
    let transport = CostCategories(name: "Транспорт",
                                   image: "car1",
                                   summ: 0.0,
                                   plannedToSpend: 0.0)
    let services = CostCategories(name: "Еда",
                                  image: "food1",
                                  summ: 0.0,
                                  plannedToSpend: 0.0)
    let other = CostCategories(name: "Медецина",
                               image: "medical1",
                               summ: 0.0,
                               plannedToSpend: 0.0)
    let entertainment = CostCategories(name: "Развлечения",
                                       image: "drink1",
                                       summ: 0.0,
                                       plannedToSpend: 0.0)
}
