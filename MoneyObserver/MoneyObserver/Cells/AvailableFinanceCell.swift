//
//  AvailableFinanceCell.swift
//  MoneyObserver
//
//  Created by Кирилл on 13.07.21.
//

import UIKit

class AvailableFinanceCell: UICollectionViewCell {

    @IBOutlet weak var financeName: UILabel!
    @IBOutlet weak var financeImageButton: UIButton!
    @IBOutlet weak var financeSumm: UILabel!
    
    weak var delegate: OpenView?
    var isNewCategory: Bool = false
    
    private var isDragging = false
    var view = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        financeImageButton.layer.cornerRadius = financeImageButton.layer.frame.height / 2
       // financeImageButton.isUserInteractionEnabled = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        initialSetupCell()
    }
    
    func initialSetupCell() {
        isNewCategory = false
        financeName.isHidden = false
        financeSumm.isHidden = false
        financeImageButton.layer.borderWidth = 0
        financeImageButton.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
        financeImageButton.setImage(UIImage(systemName: "dollarsign.circle"), for: .normal)
        financeImageButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    func setupCell(category: FinanceCategories) {
        financeName.text = category.name
        financeSumm.text = "\(category.summ.format())"
        financeImageButton.setImage(UIImage(named: category.image), for: .normal) 
    }
    
    func setupNewCategoryCell() {
        financeName.isHidden = true
        financeImageButton.layer.borderWidth = 2
        financeImageButton.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        financeImageButton.backgroundColor = .clear
        financeImageButton.setImage(UIImage(systemName: "plus"), for: .normal)
        financeImageButton.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        financeSumm.isHidden = true
    }
    
    func setupFromAddMoney(category: FinanceCategories) {
        financeName.isHidden = false
        financeName.text = category.name
        financeSumm.isHidden = true
        financeImageButton.isEnabled = false
    }
    
    func setupCellForIconSetView(type: TypesOfCategoryes, icon: String) {
        financeName.isHidden = true
        financeSumm.isHidden = true
        financeImageButton.isEnabled = false
        financeImageButton.setImage(UIImage(named: icon), for: .disabled)
        switch type {
        case .finances:
            financeImageButton.backgroundColor = #colorLiteral(red: 0.2761262953, green: 0.4753057361, blue: 0.4362707138, alpha: 0.8470588235)
        case .costs:
            financeImageButton.backgroundColor = #colorLiteral(red: 1, green: 0.6705882353, blue: 0, alpha: 1)
        }
    }
    
    @IBAction func pushNewCategoryView(_ sender: Any) {
        if isNewCategory {
            print(isNewCategory)
            isNewCategory = false
            delegate?.pushView(forCategory: .finances)
        } else {
            print(isNewCategory)
            delegate?.openFinanceOperationView(fromCategory: financeName.text!)
        }
    }
    

}

//extension AvailableFinanceCell {
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let touch = touches.first else { return }
//        let location = touch.location(in: self)
//        
//        if financeImageButton.bounds.contains(location) {
//            isDragging = true
//        }
//        
//    }
//    
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard isDragging, let touch = touches.first else { return }
//        
//        let location = touch.location(in: view)
//        
//        self.frame.origin.x = location.x - (self.frame.size.width / 2)
//        self.frame.origin.y = location.y - (self.frame.size.height / 2)
//    }
//    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//         isDragging = false
//    }
//}
