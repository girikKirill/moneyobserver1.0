//
//  CostCategories.swift
//  MoneyObserver
//
//  Created by Кирилл on 12.07.21.
//

import Foundation
import RealmSwift

class CostCategories: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var summ: Double = 0.0
    @objc dynamic var plannedToSpend: Double = 0.0

    convenience init(name: String, image: String, summ: Double, plannedToSpend: Double) {
        self.init()
        self.name = name
        self.image = image
        self.summ = summ
        self.plannedToSpend = plannedToSpend
    }
}
