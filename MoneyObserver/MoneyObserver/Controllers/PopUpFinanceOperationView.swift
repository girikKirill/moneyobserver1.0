//
//  PopUpFinanceOperationView.swift
//  MoneyObserver
//
//  Created by Кирилл on 18.07.21.
//

import UIKit

class PopUpFinanceOperationView: UIViewController {

    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var summTF: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    
    weak var delegate: PopUpDelegate?
    var indexPath = 0
    var type: TypesOfCategoryes?
    var isTransfer = false
    var isSpend = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        summTF.delegate = self
        summTF.keyboardType = .decimalPad
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func setupFinOperationsPopUp(fromWhere: String = "Доход", whereTo: String, isTransfer: Bool, isSpend: Bool) {
        self.isTransfer = isTransfer
        self.isSpend = isSpend
        
        fromLabel.text = fromWhere
        toLabel.text = whereTo

        popUpView.layer.borderWidth = 1
        popUpView.layer.borderColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
        popUpView.layer.cornerRadius = 15

        cancelButton.layer.cornerRadius = 10
        saveButton.layer.cornerRadius = 10
        saveButton.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 0.6579749367)
    }

    @IBAction func cancelAction(_ sender: Any) {
        delegate?.cancelAction()
        dismiss(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        guard let from = fromLabel.text, let to = toLabel.text else { return }
        guard let summ = summTF.text, !summ.isEmpty else {
            print("Введите сумму")
            return
        }

        let formatter = NumberFormatter()
        formatter.decimalSeparator = ","
        let grade = formatter.number(from: summ)
        guard let doubleSumm = grade?.doubleValue, doubleSumm != 0.0 else { return }
        
        var date: String = ""
        date = date.getCurrentDate()
        
        let finOperation = FinancialOperations(fromWhere: from, whereTo: to, summ: doubleSumm, date: date, isSpend: isSpend, isTransfer: isTransfer)
        dismiss(animated: true)
        switch type {
        case .costs:
            delegate?.saveAction(type: .costs, operation: finOperation, indexPath: nil)
        case .finances:
            delegate?.saveAction(type: .finances, operation: finOperation, indexPath: nil)
        case .none:
            delegate?.saveAction(type: nil, operation: finOperation, indexPath: indexPath)
        }
    }
}

extension PopUpFinanceOperationView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        var allowedCharacters = CharacterSet.decimalDigits
//        allowedCharacters.insert(".")
//        let characterSet = CharacterSet(charactersIn: string)
//        return allowedCharacters.isSuperset(of: characterSet)
        let text = (textField.text ?? "") as NSString
            let newText = text.replacingCharacters(in: range, with: string)
            if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,2})?$", options: .caseInsensitive) {
                return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
            }
            return false
    }
}
