//
//  Protocols.swift
//  MoneyObserver
//
//  Created by Кирилл on 14.07.21.
//

import Foundation
import UIKit

protocol OpenView: AnyObject {
    func pushView(forCategory: TypesOfCategoryes)
    func openFinanceOperationView(fromCategory: String)
}

protocol SaveCategories: AnyObject {
    func saveNewFinance(category: FinanceCategories)
    func saveNewCost(category: CostCategories)
}

protocol PopUpDelegate: AnyObject {
    func cancelAction()
    func saveAction(type: TypesOfCategoryes?, operation: FinancialOperations, indexPath: Int?)
}

protocol SetImage: AnyObject {
    func saveSelected(image: String)
}

extension String {
    func getCurrentDate() -> String {
        let date = Date()
        let formatter =  DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "d MMMM y"
        return formatter.string(from: date)
    }
}

extension Double {
    func format() -> String {
        return String(format: "%.2f", self)
    }
}

//extension UIViewController {
//    func addAler(alertTitle: String? = nil, alertMessage: String? = nil, ) {
//        
//    }
//}

