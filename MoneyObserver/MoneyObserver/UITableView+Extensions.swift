//
//  UITableView+Extensions.swift
//  MoneyObserver
//
//  Created by Кирилл on 12.07.21.
//

import Foundation
import UIKit

extension UICollectionView {
    func registerCell(_ cellClass: AnyClass) {
        let nib = UINib(nibName: String(describing: cellClass.self), bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: String(describing: cellClass.self))
    }
    
    func setupDelegateData(_ controller: UIViewController) {
        self.delegate = controller as? UICollectionViewDelegate
        self.dataSource = controller as? UICollectionViewDataSource
        //self.tableFooterView = UIView()
    }
    
    func setupSizeForItem(_ collection: UICollectionView) -> CGFloat {
        
        var itemsPerScreen: CGFloat = 4
        
        if self.frame.width < 375 {
            itemsPerScreen = 3
        }
        let paddingWidth = 10 * (itemsPerScreen + 1)
        let avaibleWidth = self.frame.width - paddingWidth
        let widthPerItem = avaibleWidth / itemsPerScreen
        return widthPerItem
    }
}

extension UITableView {
    func registerCell(_ cellClass: AnyClass) {
        let nib = UINib(nibName: String(describing: cellClass.self), bundle: nil)
        self.register(nib, forCellReuseIdentifier: String(describing: cellClass.self))
    }
    
    func setupDelegateData(_ controller: UIViewController) {
        self.delegate = controller as? UITableViewDelegate
        self.dataSource = controller as? UITableViewDataSource
        self.tableFooterView = UIView()
        self.separatorStyle = .none
    }
}
