//
//  IconsSetVIew.swift
//  MoneyObserver
//
//  Created by Кирилл on 15.07.21.
//

import UIKit

class IconsSetView: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textLabel: UILabel!
    
    let categoryIcons = CategoryIcons()
    var categoryType: TypesOfCategoryes = .finances
    
    weak var delegate: SetImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.setupDelegateData(self)
        collectionView.registerCell(AvailableFinanceCell.self)
        textLabel.textColor = .white
    }
    
    func setupView(categoryType: TypesOfCategoryes) {
        switch categoryType {
        case .finances:
            self.view.backgroundColor = #colorLiteral(red: 0.2667813897, green: 0.4937652349, blue: 0.4428756833, alpha: 1)
            collectionView.backgroundColor = #colorLiteral(red: 0.2667813897, green: 0.4937652349, blue: 0.4428756833, alpha: 1)
        case .costs:
            self.view.backgroundColor = #colorLiteral(red: 1, green: 0.7482267022, blue: 0.2770791948, alpha: 1)
            collectionView.backgroundColor = #colorLiteral(red: 1, green: 0.7482267022, blue: 0.2770791948, alpha: 1)
        }
    }
}

// CollectionView delegate & dataSource

extension IconsSetView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        delegate?.saveSelected(image: categoryIcons.financeIcons[indexPath.row])
    }
}

extension IconsSetView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryIcons.financeIcons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvailableFinanceCell.self), for: indexPath)
        guard let iconCell = cell as? AvailableFinanceCell else { return cell }
        switch categoryType {
        case .finances:
            iconCell.setupCellForIconSetView(type: .finances, icon: categoryIcons.financeIcons[indexPath.row])
        case .costs:
            iconCell.setupCellForIconSetView(type: .costs, icon: categoryIcons.financeIcons[indexPath.row])
        }
        return iconCell
    }
}

// CollectionView FlowLayout

extension IconsSetView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        
        let itemsPerScreen: CGFloat = 4
        let paddingWidth = 5 * (itemsPerScreen + 1)
        let avaibleWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = avaibleWidth / itemsPerScreen
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
