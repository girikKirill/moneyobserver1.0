//
//  FinancialOperations.swift
//  MoneyObserver
//
//  Created by Кирилл on 18.07.21.
//

import Foundation
import RealmSwift

class FinancialOperations: Object {
    @objc dynamic var fromWhere: String = ""
    @objc dynamic var whereTo: String = ""
    @objc dynamic var summ: Double = 0.0
    @objc dynamic var date: String = ""
    @objc dynamic var isSpend: Bool = false
    @objc dynamic var isTransfer: Bool = false




    convenience init(fromWhere: String, whereTo: String, summ: Double, date: String, isSpend: Bool, isTransfer: Bool) {
        self.init()
        self.fromWhere = fromWhere
        self.whereTo = whereTo
        self.summ = summ
        self.date = date
        self.isSpend = isSpend
        self.isTransfer = isTransfer
    }
}
