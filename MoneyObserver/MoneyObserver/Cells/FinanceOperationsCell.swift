//
//  FinanceOperationsCell.swift
//  MoneyObserver
//
//  Created by Кирилл on 20.07.21.
//

import UIKit

class FinanceOperationsCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var whereLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(operation: FinancialOperations) {
        fromLabel.text = operation.fromWhere
        whereLabel.text = operation.whereTo
        containerView.layer.borderWidth = 1
        containerView.layer.cornerRadius = 20
        containerView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        arrowImage.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            
        if operation.isSpend {
            summLabel.text = "- \(operation.summ) BYN"
            summLabel.textColor = #colorLiteral(red: 0.7624632716, green: 0, blue: 0, alpha: 1)
//            containerView.layer.borderColor = #colorLiteral(red: 0.7624632716, green: 0, blue: 0, alpha: 1)
            fromLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            whereLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            
        } else if operation.isTransfer {
            summLabel.text = "\(operation.summ) BYN"
            summLabel.textColor = #colorLiteral(red: 0.1596950591, green: 0.4536509514, blue: 0.6492139697, alpha: 1)
//            containerView.layer.borderColor = #colorLiteral(red: 0.1596950591, green: 0.4536509514, blue: 0.6492139697, alpha: 1)
            fromLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            whereLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        } else {
            summLabel.text = "+ \(operation.summ) BYN"
            summLabel.textColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
//            containerView.layer.borderColor = #colorLiteral(red: 0.2196078431, green: 0.4039215686, blue: 0.3607843137, alpha: 1)
            fromLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            whereLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        }
    }
    

    
}
