//
//  CategoryIcons.swift
//  MoneyObserver
//
//  Created by Кирилл on 28.07.21.
//

import Foundation

struct CategoryIcons {
    let financeIcons: [String] = ["creditCard1",
                                  "wallet1",
                                  "wallet2",
                                  "car1",
                                  "drink1",
                                  "food1",
                                  "gift1",
                                  "medical1",
                                  "pets1",
                                  "shopping1",
                                  "skincare1",
                                  "travels1",
                                  "gym1",
                                  "balloons1",
                                  "cart1",
                                  "pill1",
                                  "video-player1",
                                  "baby-stroller1",
                                  "coffee-break1",
                                  "home1",
                                  "internet1",
                                  "light-bulb1",
                                  "outline"]
}
